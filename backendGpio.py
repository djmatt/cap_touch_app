#!/usr/bin/python3
import Adafruit_MPR121.MPR121 as MPR121
import RPi.GPIO as GPIO
import time
import pygame
from pygame.locals import *


pygame.init()
screen = pygame.display.set_mode((200,200))

aLED = (22, 23, 5, 6, 13, 16, 20, 21)
aLEDmO = (22, 23, 5, 6, 13, 16, 20, 21) 



GPIO.setmode(GPIO.BCM)
GPIO.setup(aLED, GPIO.OUT)
GPIO.setup(17, GPIO.IN)
GPIO.setup(18, GPIO.OUT)
GPIO.output(aLED, GPIO.LOW)


pLED = []
for i in range(0, 8):
    pLED.append(GPIO.PWM(aLED[i], 50))
    pLED[i].start(0)
    
tModeSelect = "Normal"
tModeLightLED = "Normal"


def main():

    global tModeSelect
    global tModeLightLED

    tModeSelect = "Normal"
    tModeLightLED = "Normal"
        
    nPower = False
    cap = MPR121.MPR121()
    
    if not cap.begin():
        print ('Error initializing MPR121.  Check your wiring!')
        sys.exit(1)
    
    last_touched = cap.touched()

    nSwitch = True
    nLED = True

    nMastInd = 0
    lFil =[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    lIndex =[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    lReg =[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    lPress =[False, False, False, False, False, False, False, False]
    
    lA = [[0, 0, 0, 0, 0],[0, 0, 0, 0, 0],[0, 0, 0, 0, 0],[0, 0, 0, 0, 0],[0, 0, 0, 0, 0],[0, 0, 0, 0, 0],[0, 0, 0, 0, 0],[0, 0, 0, 0, 0]]
    


    
    try:
        while True:

            
            for event in pygame.event.get():
                if (event.type == KEYDOWN):
                    if (event.key == K_n):
                        tModeSelect = "Normal"
                        tModeLightLED = "Normal"
                        nActiveLED = 10
                    if (event.key == K_t):
                        tModeSelect = "Toggle"
                        nActiveLED = 10
                    if (event.key == K_c):
                        tModeSelect = "Checked"
                        nActiveLED = 0
                    if (event.key == K_p):
                        tModeLightLED = "PWM"

            

            for nMastInd in range(0, 8):
                time.sleep(0.01)
                lFil[nMastInd] = cap.filtered_data(nMastInd)

                if lPress[nMastInd] == False :
                    if (lFil[nMastInd] - lReg[nMastInd]) >= -2:
                        lA[nMastInd][lIndex[nMastInd]] = lFil[nMastInd]
                        lReg[nMastInd] =  max(lA[nMastInd])
                        if lIndex[nMastInd] == 4: lIndex[nMastInd]=0
                        else: lIndex[nMastInd] +=1
                    else:
                        lPress[nMastInd] = True

                        if nPower == True:
                            if nMastInd == 0:
                                PowerOFF()
                                nPower = False

                                if tModeSelect == "Normal":
                                    nActiveLED = 10
                                elif tModeSelect == "Checked":
                                    nActiveLED = 0

                                    
                            else:
                                if tModeSelect == "Normal":
                                    ModeNormal(nMastInd, True)
                                elif tModeSelect == "Toggle":
                                    nActiveLED = ModeToggle(nMastInd, True, nActiveLED)
                                elif tModeSelect == "Checked":
                                    nActiveLED = ModeChecked(nMastInd, True, nActiveLED)
                        else:
                            if nMastInd == 0:
                                OnOffLED(0, True)
                                nPower = True
  
                else:
                    if (lFil[nMastInd] - lReg[nMastInd]) >= -2:

                        if nMastInd != 0:
                            if nPower == True:
                                if tModeSelect == "Normal":
                                    ModeNormal(nMastInd, False)
                                elif tModeSelect == "Toggle":
                                    nActiveLED = ModeToggle(nMastInd, False, nActiveLED)
                                elif tModeSelect == "Checked":
                                    nActiveLED = ModeChecked(nMastInd, False, nActiveLED)
                        lPress[nMastInd] = False

    except KeyboardInterrupt:
        print("Interrupted!")

    GPIO.cleanup()
    for i in range(0, 8):
        pLED[i].stop()


def ModeNormal(nLED, bOnOff):
    if bOnOff == True:
        OnOffLED(nLED, True)
    else:
        OnOffLED(nLED, False)
        

def ModeToggle(nLED, bOnOff, nActiveLED):
    if bOnOff == True:
        if nActiveLED != 10:
            OnOffLED(nActiveLED, False)
        if nActiveLED != nLED:
            OnOffLED(nLED, True)
            nActiveLED = nLED
        else:
            nActiveLED = 10
    return nActiveLED


def ModeChecked(nLED, bOnOff, nActiveLED):
    if bOnOff == True:
        #print("nLED : {0:b} - nActiveLED : {1:b}".format((1 << nLED), nActiveLED))
        if (((1 << nLED) & nActiveLED) == 0):
            OnOffLED(nLED, True)
            nActiveLED = nActiveLED | (1 << nLED)
        else:
            OnOffLED(nLED, False)
            nActiveLED = nActiveLED ^ (1 << nLED)
    return nActiveLED


def OnOffLED(nIDLED, bONOFF):
    if tModeLightLED == "Normal":
        if bONOFF == True:
            pLED[nIDLED].start(100)
        else:
            pLED[nIDLED].start(0)
    elif tModeLightLED == "PWM":
        if bONOFF == True:
            for dc in range(0, 101, 1):
                pLED[nIDLED].ChangeDutyCycle(dc)
                time.sleep(0.001)
        else:
            for dc in range(100, -1, -1):
                pLED[nIDLED].ChangeDutyCycle(dc)
                time.sleep(0.001)

def PowerOFF():
    for i in range(0, 8):
        pLED[i].start(0)

main()
